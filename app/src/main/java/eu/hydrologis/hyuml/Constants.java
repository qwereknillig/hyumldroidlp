/*
 * HyUML - Android application to facilitate the use of the yuml.me service
 * Copyright (C) 2014  HyUML
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.hydrologis.hyuml;


/**
 * Various constants.
 * 
 * @author Andrea Antonello (www.hydrologis.com)
 */
@SuppressWarnings("nls")
public interface Constants {

    public final String DIRECTORYBROWSER = "eu.hydrologis.hyuml.DIRECTORYBROWSE";

    public final String ID = "ID";
    public final String INTENT_ID = "INTENT_ID";
    public final String EXTENTION = "EXTENTION";
    public final String SHOWHIDDEN = "SHOWHIDDEN";
    public final String PATH = "PATH";
    public final String INITIALPATH = "INITIALPATH";

}

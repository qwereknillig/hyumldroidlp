/*
 * Geopaparazzi - Digital field mapping on Android based devices
 * Copyright (C) 2010  HydroloGIS (www.hydrologis.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.hydrologis.hyuml;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class DirectoryBrowserActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private List<String> items = null;
    private String intentId;
    private FileFilter fileFilter;

    private File currentDir;
    private boolean doFolder;
    private boolean doHidden;
    private String initialPath;
    private ListView listview;

    @Override
    public void onCreate( Bundle icicle ) {
        super.onCreate(icicle);
        setContentView(R.layout.browse);

        setSupportActionBar((Toolbar) findViewById(R.id.awesome_action_bar));

        listview = (ListView) findViewById(android.R.id.list);
        listview.setOnItemClickListener(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            initialPath = extras.getString(Constants.INITIALPATH);
            intentId = extras.getString(Constants.INTENT_ID);
            doHidden = extras.getBoolean(Constants.SHOWHIDDEN, false);

            fileFilter = new FileFilter(){
                public boolean accept( File pathname ) {
                    return true;
                }
            };
        }

        Button okButton = (Button) findViewById(R.id.okbutton);
        if (doFolder) {
            okButton.setOnClickListener(new OnClickListener(){
                public void onClick( View v ) {
                    String absolutePath = currentDir.getAbsolutePath();
                    handleIntent(absolutePath);
                    finish();
                }
            });
        } else {
            okButton.setEnabled(false);
        }
        Button upButton = (Button) findViewById(R.id.upbutton);
        upButton.setOnClickListener(new OnClickListener(){
            public void onClick( View v ) {
                goUp();
            }
        });

        File folder = new File(initialPath);
        getFiles(folder, folder.listFiles(fileFilter));
    }
    private void handleIntent( String absolutePath ) {
        if (intentId != null) {
            Intent intent = new Intent(intentId);
            intent.putExtra(Constants.PATH, absolutePath);
            startActivity(intent);
        } else {
            Intent intent = new Intent((String) null);
            intent.putExtra(Constants.PATH, absolutePath);
            setResult(Activity.RESULT_OK, intent);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View v, int position, long id ) {
        int selectedRow = (int) id;
        File file = new File(items.get(selectedRow));
        if (file.isDirectory()) {
            File[] filesArray = file.listFiles(fileFilter);
            if (filesArray != null) {
                currentDir = file;
                getFiles(currentDir, filesArray);
            } else {
                filesArray = currentDir.listFiles(fileFilter);
                getFiles(currentDir, filesArray);
            }
        } else {
            String absolutePath = file.getAbsolutePath();
            handleIntent(absolutePath);
            finish();
        }
    }

    private void goUp() {
        File tmpDir = currentDir.getParentFile();
        if (tmpDir != null && tmpDir.exists()) {
            if (tmpDir.canRead()) {
                currentDir = tmpDir;
            }
        }
        getFiles(currentDir, currentDir.listFiles(fileFilter));
    }

    private void getFiles( File parent, File[] files ) {
        Arrays.sort(files);
        currentDir = parent;
        items = new ArrayList<String>();
        for( File file : files ) {
            if (!doHidden && file.getName().startsWith(".")) {
                continue;
            }
            items.add(file.getPath());
        }
        ArrayAdapter<String> fileList = new ArrayAdapter<String>(this, R.layout.browse_file_row, items);
        listview.setAdapter(fileList);
    }
}
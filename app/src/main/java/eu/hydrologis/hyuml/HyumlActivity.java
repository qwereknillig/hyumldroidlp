/*
 * HyUML - Android application to facilitate the use of the yuml.me service
 * Copyright (C) 2014  HyUML
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.hydrologis.hyuml;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class HyumlActivity extends ActionBarActivity implements ImageUtils.bitmapLoadedListener, View.OnClickListener {

    private Spinner typeSpinner;

    private ImageUtils.Diagram currentType = ImageUtils.Diagram.CLASS;

    private EditText scriptText;

    private ImageView imageView;
    private File hyumlDir;
    private boolean canSave;
    private Spinner orientSpinner;
    private String currentOrientation;
    private Spinner scaleSpinner;
    private String currentScale;
    private Bitmap currentBitmap;

    private static final int LOAD_FILE_CODE = 666;
    protected static final String LAST_SAVED_FILEKEY = "LAST_SAVED_FILEKEY";

    /** Called when the activity is first created. */
    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);



        setSupportActionBar((Toolbar) findViewById(R.id.awesome_action_bar));

        imageView = (ImageView) findViewById(R.id.imageView);
        scriptText = (EditText) findViewById(R.id.scriptfield);

        Button renderButton = (Button) findViewById(R.id.renderButton);
        renderButton.setOnClickListener(this);

        typeSpinner = (Spinner) findViewById(R.id.typeSpinner);
        ArrayAdapter< ? > typeSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.array_types,
                android.R.layout.simple_spinner_item);
        typeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(typeSpinnerAdapter);
        typeSpinner.setSelection(ImageUtils.Diagram.CLASS.getIndex());
        currentType = ImageUtils.Diagram.CLASS;
        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            public void onItemSelected( AdapterView< ? > arg0, View arg1, int arg2, long arg3 ) {
                Object selectedItem = typeSpinner.getSelectedItem();
                currentType = ImageUtils.Diagram.fromName(selectedItem.toString());
            }
            public void onNothingSelected( AdapterView< ? > arg0 ) {
            }
        });

        final String[] labels = getResources().getStringArray(R.array.array_orientlabels);
        final String[] values = getResources().getStringArray(R.array.array_orientvalues);
        orientSpinner = (Spinner) findViewById(R.id.orientSpinner);
        ArrayAdapter< ? > orientSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.array_orientlabels,
                android.R.layout.simple_spinner_item);
        orientSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        orientSpinner.setAdapter(orientSpinnerAdapter);
        orientSpinner.setSelection(1);
        currentOrientation = orientSpinner.getSelectedItem().toString();
        orientSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            public void onItemSelected( AdapterView< ? > arg0, View arg1, int arg2, long arg3 ) {
                Object selectedItem = orientSpinner.getSelectedItem();
                currentOrientation = selectedItem.toString();
                if (currentOrientation.equals(labels[0])) {
                    currentOrientation = values[0];
                } else {
                    currentOrientation = values[1];
                }
            }
            public void onNothingSelected( AdapterView< ? > arg0 ) {
            }
        });

        scaleSpinner = (Spinner) findViewById(R.id.scaleSpinner);
        ArrayAdapter< ? > scaleSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.array_scales,
                android.R.layout.simple_spinner_item);
        scaleSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        scaleSpinner.setAdapter(scaleSpinnerAdapter);
        scaleSpinner.setSelection(4);
        currentScale = scaleSpinner.getSelectedItem().toString();
        scaleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            public void onItemSelected( AdapterView< ? > arg0, View arg1, int arg2, long arg3 ) {
                Object selectedItem = scaleSpinner.getSelectedItem();
                currentScale = selectedItem.toString().replaceFirst("%", "");
            }
            public void onNothingSelected( AdapterView< ? > arg0 ) {
            }
        });

        String state = Environment.getExternalStorageState();
        boolean mExternalStorageAvailable;
        boolean mExternalStorageWritable;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mExternalStorageAvailable = mExternalStorageWritable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            mExternalStorageAvailable = true;
            mExternalStorageWritable = false;
        } else {
            mExternalStorageAvailable = mExternalStorageWritable = false;
        }
        if (mExternalStorageAvailable && mExternalStorageWritable) {
            File sdcardDir = Environment.getExternalStorageDirectory();// new
            hyumlDir = new File(sdcardDir.getAbsolutePath() + File.separator + "hyuml");
            if (!hyumlDir.exists()) {
                hyumlDir.mkdirs();
            }
            canSave = true;
        } else {
            openDialog("The external card is not writable, it will not be possible to use the saving functions.");
            canSave = false;
        }

    }

    public boolean onCreateOptionsMenu( Menu menu ) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch( item.getItemId() ) {
            case R.id.action_about:
                openDialog("Hyuml is created by HydroloGIS and is a GPL licensed application.");
                return true;
            case R.id.action_load:
                Intent browseIntent = new Intent(Constants.DIRECTORYBROWSER);
                browseIntent.putExtra(Constants.INITIALPATH, hyumlDir.getAbsolutePath());
                startActivityForResult(browseIntent, LOAD_FILE_CODE);
                return true;
            case R.id.action_save:
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String lastSavedFile = preferences.getString(LAST_SAVED_FILEKEY, "");
                if (lastSavedFile.length() == 0) {
                    saveAs();
                } else {
                    String script = scriptText.getText().toString();
                    try {
                        FileUtilities.writeFile(script, new File(lastSavedFile));
                        Toast.makeText(HyumlActivity.this, "File saved", Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(HyumlActivity.this, "An error occurred while saving the file", Toast.LENGTH_LONG).show();
                    }
                }
                return true;
            case R.id.action_save_as:
                saveAs();
                return true;
            case R.id.action_save_image:
                saveImageAs();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveAs() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Save");
        alert.setMessage("Enter a filename to save the script to");
        final EditText input = new EditText(this);
        alert.setView(input);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
            public void onClick( DialogInterface dialog, int whichButton ) {
                Editable value = input.getText();
                File file = new File(hyumlDir + File.separator + value.toString());
                String script = scriptText.getText().toString();
                try {
                    FileUtilities.writeFile(script, file);
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(LAST_SAVED_FILEKEY, file.getAbsolutePath());
                    editor.commit();
                    Toast.makeText(HyumlActivity.this, "File saved", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(HyumlActivity.this, "An error occurred while saving the file", Toast.LENGTH_LONG).show();
                }
            }
        });
        alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(){
            public void onClick( DialogInterface dialog, int whichButton ) {
            }
        });
        alert.show();
    }

    private void saveImageAs() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Save Image");
        alert.setMessage("Enter a filename to save the image to");
        final EditText input = new EditText(this);
        alert.setView(input);
        alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
            public void onClick( DialogInterface dialog, int whichButton ) {
                if (currentBitmap == null) {
                    openDialog("No image available yet.");
                    return;
                }
                try {
                    Editable value = input.getText();
                    File file = new File(hyumlDir + File.separator + value.toString());

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    currentBitmap.compress(Bitmap.CompressFormat.PNG, 80, bytes);
                    FileOutputStream fo = new FileOutputStream(file);
                    fo.write(bytes.toByteArray());

                    Toast.makeText(HyumlActivity.this, "Image saved", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(HyumlActivity.this, "An error occurred while saving the file", Toast.LENGTH_LONG).show();
                }
            }
        });
        alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(){
            public void onClick( DialogInterface dialog, int whichButton ) {
            }
        });
        alert.show();
    }
    private void openDialog( String msg ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(HyumlActivity.this);
        builder.setMessage(msg).setCancelable(false).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
            public void onClick( DialogInterface dialog, int id ) {
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
        super.onActivityResult(requestCode, resultCode, data);
        switch( requestCode ) {
        case (LOAD_FILE_CODE): {
            if (resultCode == Activity.RESULT_OK) {
                String chosenFolderToLoad = data.getStringExtra(Constants.PATH);
                if (chosenFolderToLoad != null && new File(chosenFolderToLoad).exists()) {
                    String readFile;
                    try {
                        readFile = FileUtilities.readFile(chosenFolderToLoad);
                        scriptText.setText(readFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
        }
        }
    }

    @Override
    public void BitmapLoaded(Bitmap b) {
        currentBitmap = b;
    }

    public void onClick( View v ) {
        String script = scriptText.getText().toString();
        try {
            ImageUtils.getImageAsync(this, v.getContext(), imageView, script, currentType, currentOrientation, currentScale);
        } catch (Exception e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(HyumlActivity.this);
            builder.setMessage("An error occurred while generating the diagram.").setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
                        public void onClick( DialogInterface dialog, int id ) {
                        }
                    });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            e.printStackTrace();
        }
    }
}
/*
 * Copyright 2010 Alternate Computing Solutions Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.hydrologis.hyuml;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.StringTokenizer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.ImageViewBitmapInfo;
import com.koushikdutta.ion.Ion;

/**
 * Image utilities
 */
@SuppressWarnings("nls")
public class ImageUtils {

    private static final String YUML_ME_BASEURL = "http://yuml.me/diagram/";

    public enum Diagram {
        CLASS("class", 0), ACTIVITY("activity", 1), USECASE("usecase", 1);

        private final String value;
        private final int index;

        Diagram( String value, int index ) {
            this.value = value;
            this.index = index;
        }

        public String getName() {
            return value;
        }

        public int getIndex() {
            return index;
        }

        public static Diagram fromIndex( int index ) {
            switch( index ) {
            case 0:
                return Diagram.CLASS;
            case 1:
                return Diagram.ACTIVITY;
            case 2:
                return Diagram.USECASE;
            default:
                break;
            }
            return Diagram.CLASS;
        }

        public static Diagram fromName( String name ) {
            if (name.equals(CLASS.getName())) {
                return Diagram.CLASS;
            } else if (name.equals(ACTIVITY.getName())) {
                return Diagram.ACTIVITY;
            } else if (name.equals(USECASE.getName())) {
                return Diagram.USECASE;
            }
            return Diagram.CLASS;
        }
    }

    /**
     * populates the image of the given diagram
     * @param currentScale 
     * @param currentOrientation 
     * @return 
     * 
     * @throws Exception 
     */
    public static Bitmap getImage( String script, Diagram diagram, String orientation, String scale ) throws Exception {
        String urlString = getUrlString(script, diagram, orientation, scale, false);
        urlString = urlString.replaceAll(" ", "%20");

        Log.d("IMAGEUTILS", "URL CALLED: " + urlString);
        URL url = new URL(urlString);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStream is = connection.getInputStream();
        Bitmap image = BitmapFactory.decodeStream(is);
        return image;
    }

    public static void getImageAsync(final bitmapLoadedListener l, Context con, ImageView view, String script, Diagram diagram, String orientation, String scale ) {
        String urlString = getUrlString(script, diagram, orientation, scale, false);
        urlString = urlString.replaceAll(" ", "%20");
        Ion.with(con).load(urlString).intoImageView(view).withBitmapInfo().setCallback(new FutureCallback<ImageViewBitmapInfo>() {
            @Override
            public void onCompleted(Exception e, ImageViewBitmapInfo result) {
                l.BitmapLoaded(result.getBitmapInfo().bitmap);
            }
        });
    }

    public static String getHtmlImgTag( String script, Diagram diagram, String orientation, String scale, boolean scrubby )
            throws Exception {
        String urlString = getUrlString(script, diagram, orientation, scale, scrubby);
        urlString = "<img src=\"" + urlString + "\" />";
        return urlString;
    }

    private static String getUrlString( String script, Diagram diagram, String orientation, String scale, boolean scrubby ) {
        if (script.length() == 0) {
            return "";
        }
        StringTokenizer st = new StringTokenizer(script.trim(), "\n");
        StringBuilder buffer = new StringBuilder();
        while( st.hasMoreTokens() ) {
            String trim = st.nextToken().trim();
            if (trim.length() > 0 && !trim.startsWith("#")) {
                buffer.append(trim);
                if (diagram != Diagram.ACTIVITY)
                    if (st.hasMoreTokens() && !trim.trim().endsWith(",")) {
                        buffer.append(",");
                    }
            }
        }
        StringBuilder sb = new StringBuilder();
        if (orientation != null) {
            sb.append(orientation);
        }
        if (scale != null) {
            if (orientation != null) {
                sb.append(";");
            }
            sb.append("scale:").append(scale);
        }
        if (sb.length() > 0) {
            sb.append("/");
        }

        String scruffy = "";
        if (scrubby) {
            scruffy = "scruffy;";
        }
        String urlString = YUML_ME_BASEURL + scruffy + sb.toString() + diagram.getName() + "/" + buffer.toString();
        urlString = urlString + ".png";
        return urlString;
    }


    public interface bitmapLoadedListener {
        public void BitmapLoaded(Bitmap b);
    }
}
